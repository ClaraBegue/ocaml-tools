module BChar = Internal_BChar

module BArray =
struct
  include Internal_BArray
  module Nat =
  struct
    type nat = t
    include Internal_BArray_Nat
  end
end

module BNat = BArray.Nat

module OfB =
struct
  type stream = BinUtils.stream
  type 'a t = 'a BinUtils.load
  include Internal_OfB
end

module ToB =
struct
  type stream = BinUtils.stream
  type 'a t = 'a BinUtils.dump
  include Internal_ToB
end

module IoB =
struct
  type stream = BinUtils.stream
  type 'a t = 'a BinUtils.o3s
  type 'a b = 'a BinUtils.o3b
  include Internal_IoB
end

module OfBStream =
struct
  include Internal_OfBStream
end

type 'a br = 'a OfBStream.t

module ToBStream =
struct
  include Internal_ToBStream
end

type 'a bw = 'a ToBStream.t

let barray_of_br (br:'a br) (bin:BArray.t) : 'a =
  let cha = OfBStream.Channel.open_barray bin in
  let a = br cha in
  OfBStream.Channel.close_barray cha;
  a

let barray_of_bw (bw:'a bw) (a:'a) : BArray.t =
  let cha = ToBStream.Channel.open_barray () in
  bw cha a;
  ToBStream.Channel.close_barray cha

let barray_of_b3 (bw, br) : (_, BArray.t) O3.o3 =
  (barray_of_bw bw, barray_of_br br)

module IoBStream =
struct
end

type 'a b3 = 'a bw * 'a br
