#NUMS=/usr/lib/ocaml/nums
#LIBS=-libs /usr/lib64/ocaml/unix
#LIBS=-lib /usr/lib/ocaml/nums
#SRCS=-Is tools,src,core,bitv,grobdd,io,conv,oops,circuits,mlbdd
NPROC=$(shell nproc)
#OcamlBuild="/home/my_name/.opam/system/bin/ocamlbuild"
OcamlBuild=${BOREAL_OPAM_ROOT}/bin/ocamlbuild
#OcamlBuild="ocamlbuild"
OB=$(OcamlBuild) -r -j $(NPROC) $(LIBS) $(SRCS)
#MV=(mv *.native *.byte bin/ &> /dev/null) || true
POST=(mv *.native *.byte bin/ &> /dev/null) || true
.PHONY: all bin tests noreg

all : ocamltools tests
	$(POST)

pre:
	rm -rf bin
	mkdir bin
	./format.sh

tests: pre
	$(OB) \
		DBBC.native \
		HGC_HBTree.native \
		tests/test_HGC_HBTree.native

ocamltools : pre
	$(OB) \
		binUtils.native \
		boundedMemoSearch.native \
		bTools.native \
		bTreeUtils.native \
		extra.native \
		graphIsoAdj.native \
		graphLA.native \
		graphWLA.native \
		h2Table.native \
		huffmanCoding.native \
		huffmanIO.native \
		internal_BArray.native \
		internal_BArray_Nat.native \
		internal_BChar.native \
		internal_IoB.native \
		internal_OfB.native \
		internal_OfBStream.native \
		internal_ToB.native \
		internal_ToBStream.native \
		intHeap.native \
		iter.native \
		memoTable.native \
		myArray.native \
		myList.native \
		o3Extra.native \
		o3.native \
		o3Utils.native \
		poly.native \
		priorityQueue.native \
		setList.native \
		sTools.native \
		tools.native \
		tree.native \
		unionFind.native

clean:
	$(OcamlBuild) -clean
	rm -rf _build
	rm -f bin/*.native bin/*.byte *.native *.d.byte

