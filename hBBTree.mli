val default_size : int
type ident = int

open BTools
module BA = BArray

type  link
and   next =
  | Leaf
  | Node of link
and   node =
  BA.t * next * next

type ('edge, 'leaf) edge =
  'edge * ('leaf, link) Tree.gnext

val unlink : link -> ident *  node

val link_cmp : link ->  link -> int
val next_cmp : next ->  next -> int
val node_cmp : node ->  node -> int

val link_equal : link ->  link -> bool
val next_equal : next ->  next -> bool
val node_equal : node ->  node -> bool

val link_hash : int -> link -> int
val next_hash : int -> next -> int
val node_hash : int -> node -> int

type 'a gnode =
  (BA.t * (unit, 'a) Tree.gnext * (unit, 'a) Tree.gnext)

val export :
  (* apply *)  ((link -> 'res) -> link -> 'res) ->
  (* recfun *) ((unit -> 'res)   gnode -> 'res) ->
    link -> 'res

val binstream_huffman_links :
  BA.t OfBStream.t ->
  (node -> link) ->
    link list OfBStream.t

val binstream_huffman_edges :
  'edge OfBStream.t ->
  'leaf OfBStream.t ->
   BA.t OfBStream.t ->
   (node -> link) ->
    ('edge, 'leaf) edge list OfBStream.t

val boutstream_huffman_links :
  BA.t ToBStream.t ->
    link list ToBStream.t

val boutstream_huffman_edges :
  'edge ToBStream.t ->
  'leaf ToBStream.t ->
   BA.t ToBStream.t ->
    ('edge, 'leaf) edge list ToBStream.t

module type Sig =
sig
  type t

  (* create n index : t *)
  val create : int -> int -> t

  val push : t -> node -> link

  val length : t -> int
  val iter : t -> (link -> unit) -> unit

  val mapreduce :
    t -> 'c -> (link -> 'b) -> ('b -> 'c -> 'c) -> 'c

  val map :
    t -> (link -> 'b) -> 'b list

  val recmap_meta : t ->
    ((link -> 'a) -> link -> 'a) ->
    ((link -> 'a) -> link -> 'a) ->
     (link -> 'a)

  val recmap : t ->
    ((link -> 'a) -> link -> 'a) ->
    (link, 'a) MemoTable.t * (link -> 'a)

  module ToSTree :
  sig
    val links : t -> link list -> Tree.stree
    val edges :
      ('edge Tree.to_stree) ->
      ('leaf Tree.to_stree) ->
      t ->
      ('edge, 'leaf) edge list Tree.to_stree
  end

  module OfSTree :
  sig
    val links : Tree.stree -> t * link list
    val edges :
      ('edge Tree.of_stree) ->
      ('leaf Tree.of_stree) ->
      Tree.stree ->
        t * ('edge, 'leaf) edge list
  end
end

module Strong : Sig
module Weak : Sig

type kl = BA.t * link array

val link_to_kl : link -> kl
val kl_to_link : kl -> link

val next_to_kl : next -> kl
val kl_to_next : kl -> next

val node_to_kl : node -> kl
val kl_to_node : kl -> node

type 'a o3kl = ('a, kl) O3.o3

type kls = (bool list) * (link list)

type 'a o3kls = ('a, kls) O3.o3s

val dump_closure : ('a -> kls -> kls) -> 'a -> kl
val load_closure : (kls -> 'a  * kls) -> kl -> 'a
val o3closure : ('a -> kls -> kls) * (kls -> 'a * kls) -> 'a o3kl

val dump_link : link -> kls -> kls
val load_link : kls -> link * kls
val o3kls_link : link o3kls

val dump_next : next -> kls -> kls
val load_next : kls -> next * kls
val o3kls_next : next o3kls

type opnext = link option
type opnode = BA.t * opnext * opnext

val next_of_opnext : opnext -> next
val opnext_of_next : next -> opnext
val o3_opnext_next : (opnext, next) O3.o3

val node_of_opnode : opnode -> node
val opnode_of_node : node -> opnode
val o3_opnode_node : (opnode, node) O3.o3
